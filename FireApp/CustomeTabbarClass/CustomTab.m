//
//  CustomTab.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 02/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "CustomTab.h"

@interface CustomTab ()

@end

@implementation CustomTab

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSBundle mainBundle]loadNibNamed:@"CustomTab" owner:self options:nil];
    _scroll.contentSize=CGSizeMake(_CustomTab.frame.size.width, 0);
    _CustomTab.frame=CGRectMake(0, _CustomTab.frame.size.height-40, _CustomTab.frame.size.height, 40);
    [_CustomTab addSubview:_scroll];
    //[self.view addSubview:_CustomTab];
    NSLog(@"%f",_CustomTab.frame.size.height);
    NSLog(@"%f",self.view.frame.size.height);

    //[self.view addSubview:_CustomTab];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)tabbtn:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 0:
        {
            
            //_hmlbl.textColor=[UIColor redColor];
            self.selectedIndex=0;
            break;
        }
        case 1:
        {
            
            //_srchlbl.textColor=[UIColor redColor];
            self.selectedIndex=1;
            break;
        }
        case 2:
        {
            
            
            self.selectedIndex=2;
            break;
        }
        case 3:
        {
            
            [_communityBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            self.selectedIndex=3;
            break;
        }
        default:
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
