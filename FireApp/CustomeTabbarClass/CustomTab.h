//
//  CustomTab.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 02/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTab : UITabBarController
@property (strong, nonatomic) IBOutlet UIView *CustomTab;
@property (strong, nonatomic) IBOutlet UIButton *frntPageBtn;
@property (strong, nonatomic) IBOutlet UIButton *outComesBtn;
@property (strong, nonatomic) IBOutlet UIButton *createLibBtn;
@property (strong, nonatomic) IBOutlet UIButton *communityBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;
- (IBAction)tabbtn:(UIButton *)sender;
@end
