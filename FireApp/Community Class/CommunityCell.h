//
//  CommunityCell.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 01/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommunityCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileImgVw;
@property (strong, nonatomic) IBOutlet UIImageView *fuelImgVw;
- (IBAction)cellBtnTap:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *detailsLbl;
@property (strong, nonatomic) IBOutlet UILabel *profileLbl;
@property (strong, nonatomic) IBOutlet UIView *sideVw;

@end
