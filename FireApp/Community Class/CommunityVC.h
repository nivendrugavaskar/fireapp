//
//  CommunityVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 01/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommunityVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
- (IBAction)addBtnTap:(id)sender;
- (IBAction)FireFeedBtnTap:(id)sender;
- (IBAction)MeBtnTap:(id)sender;
- (IBAction)MsgBtnTap:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *communityTbl;

@end
