//
//  CommunityVC.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 01/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "CommunityVC.h"
#import "CommunityCell.h"
#import "QuartzCore/QuartzCore.h"
@interface CommunityVC ()

@end

@implementation CommunityVC
@synthesize communityTbl;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.communityTbl.separatorColor=[UIColor clearColor];
    self.communityTbl.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
 //   communityTbl.contentView.backgroundColor=[UIColor lightGrayColor];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"Comcell";
    CommunityCell *cell = (CommunityCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    cell.contentView.backgroundColor=[UIColor colorWithRed:230 green:230 blue:230 alpha:0.5];
    cell.sideVw.layer.cornerRadius=5.0f;
    cell.sideVw.layer.borderWidth=1.0f;
    cell.sideVw.layer.borderColor=[UIColor clearColor].CGColor;
    cell.profileLbl.text=@"Jereme Smith";
    cell.detailsLbl.text=@"I have been in Fire since last 5 Days.";
    cell.profileImgVw.layer.cornerRadius=10.0f;
    cell.fuelImgVw.layer.cornerRadius=10.0f;
    cell.profileImgVw.image=[UIImage imageNamed:@"profile.jpg"];
    if(indexPath.row%2==0)
    {
        cell.fuelImgVw.image=[UIImage imageNamed:@"gas.png"];
    }
    else
    {
    cell.fuelImgVw.image=[UIImage imageNamed:@"color.png"];
    }
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addBtnTap:(id)sender {
}

- (IBAction)FireFeedBtnTap:(id)sender {
}

- (IBAction)MeBtnTap:(id)sender
{
    [self performSegueWithIdentifier:@"pushToCommunityMe" sender:self];
}

- (IBAction)MsgBtnTap:(id)sender
{
    [self performSegueWithIdentifier:@"pushToCommunityMsg" sender:self];
}
@end
