//
//  RegisterVC.m
//  FireApp
//
//  Created by Nivendru Gavaskar on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "RegisterVC.h"

@interface RegisterVC ()

@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTapBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)didtapNext:(id)sender {
    [self performSegueWithIdentifier:@"pushToTrackProgress" sender:nil];
}
-(IBAction)Comm:(id)sender{
    UIStoryboard *second=[UIStoryboard storyboardWithName:@"Community"bundle:nil];
    UIViewController *initial=[second instantiateInitialViewController];
    
    [self.navigationController pushViewController:initial animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
