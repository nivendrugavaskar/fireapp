//
//  CommunityDetailsVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 02/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommunityDetailsVC : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *communityImgVw;
@property (strong, nonatomic) IBOutlet UILabel *communityFrndNmLbl;
@property (strong, nonatomic) IBOutlet UILabel *communityStatusLbl;
@property (strong, nonatomic) IBOutlet UILabel *bucketListLbl;
@property (strong, nonatomic) IBOutlet UILabel *bucketListPercentageLbl;
@property (strong, nonatomic) IBOutlet UILabel *numOfFrndLbl;
@property (strong, nonatomic) IBOutlet UILabel *fuelPointLbl;
@property (strong, nonatomic) IBOutlet UILabel *momentLbl;
- (IBAction)bckBtn:(id)sender;

@end
