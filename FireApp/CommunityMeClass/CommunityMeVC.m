//
//  CommunityMeVC.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 01/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "CommunityMeVC.h"
#import "FriendRequestCell.h"
#import "MeTableCell.h"
@interface CommunityMeVC ()

@end

@implementation CommunityMeVC
@synthesize meTable;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.meTable.separatorColor=[UIColor clearColor];
    self.meTable.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   if(section==0)
   {
       return 2;
   }
    return 4;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   static NSString *cellid=@"FrndRequestCell";
    static NSString *cellid1=@"MeCell";
   if(indexPath.section==0)
   {
    FriendRequestCell  *cell=(FriendRequestCell*)[tableView dequeueReusableCellWithIdentifier:cellid forIndexPath:indexPath];
       cell.friendImgVw.image=[UIImage imageNamed:@"profile.jpg"];
       cell.friendLbl.text=@"James Bond";
       return cell;
   }
    MeTableCell *cell1=(MeTableCell*)[tableView dequeueReusableCellWithIdentifier:cellid1];
    cell1.meImgVw.image=[UIImage imageNamed:@"profile.jpg"];
    cell1.meNameLbl.text=@"Jereme";
    cell1.meDetailsLbl.text=@"Hi";
    if(indexPath.row%2==0)
    {
        cell1.meDetailsImgVw.image=[UIImage imageNamed:@"gas.png"];
    }
    else{
    cell1.meDetailsImgVw.image=[UIImage imageNamed:@"color.png"];
    }
    return cell1;
}
- (IBAction)fireFeedTap:(id)sender
{
    [self performSegueWithIdentifier:@"goBckToFeed" sender:self];
}

- (IBAction)meTap:(id)sender {
}

- (IBAction)meAddTap:(id)sender {
}

- (IBAction)messageTap:(id)sender
{
    [self performSegueWithIdentifier:@"pushToMsg" sender:self];
}
@end
