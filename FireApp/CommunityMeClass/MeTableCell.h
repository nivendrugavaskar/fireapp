//
//  MeTableCell.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 01/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *meImgVw;
@property (strong, nonatomic) IBOutlet UILabel *meNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *meDetailsLbl;
@property (strong, nonatomic) IBOutlet UIImageView *meDetailsImgVw;
@property (strong, nonatomic) IBOutlet UIButton *MeBtn;


@end
