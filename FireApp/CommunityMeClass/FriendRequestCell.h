//
//  FriendRequestCell.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 01/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendRequestCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *friendImgVw;
@property (strong, nonatomic) IBOutlet UILabel *friendLbl;
@property (strong, nonatomic) IBOutlet UIButton *acceptBtn;
@property (strong, nonatomic) IBOutlet UIButton *friendNotNowBtn;

@end
