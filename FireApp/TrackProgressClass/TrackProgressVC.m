//
//  TrackProgressVC.m
//  FireApp
//
//  Created by Nivendru Gavaskar on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "TrackProgressVC.h"

@interface TrackProgressVC ()

@end

@implementation TrackProgressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTapYes:(id)sender {
    [self performSegueWithIdentifier:@"pushToOvercome" sender:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
