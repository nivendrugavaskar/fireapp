//
//  OverComingMoneyVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverComingMoneyVC : UIViewController
- (IBAction)weightTap:(id)sender;
- (IBAction)sizeTap:(id)sender;
- (IBAction)otherTap:(id)sender;
- (IBAction)moneyTap:(id)sender;


@end
