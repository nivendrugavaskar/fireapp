//
//  msgTableCell.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 01/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface msgTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *msgTblImgVw;
@property (strong, nonatomic) IBOutlet UILabel *msgProfNmLbl;
- (IBAction)msgCellBtnTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *SideVw;

@end
