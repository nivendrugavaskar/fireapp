//
//  CommunityMsgVC.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 01/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "CommunityMsgVC.h"
#import "msgTableCell.h"
#import "QuartzCore/QuartzCore.h"
@interface CommunityMsgVC ()

@end

@implementation CommunityMsgVC
@synthesize msgTable;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.msgTable.separatorColor=[UIColor clearColor];
    self.msgTable.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
static NSString *cellid=@"msgCell";
    msgTableCell *cell=(msgTableCell*)[tableView dequeueReusableCellWithIdentifier:cellid];
    cell.SideVw.layer.cornerRadius=5.0f;
    cell.SideVw.layer.borderWidth=1.0f;
    cell.SideVw.layer.borderColor=[UIColor clearColor].CGColor;
    [cell.msgTblImgVw.layer setCornerRadius:5.0f];
    cell.msgTblImgVw.image=[UIImage imageNamed:@"profile.jpg"];
    cell.msgProfNmLbl.text=@"Jereme Smith";
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)fireFeedTap:(id)sender {
    [self performSegueWithIdentifier:@"goBckToFireFeed" sender:self];
}

- (IBAction)meTap:(id)sender
{
    [self performSegueWithIdentifier:@"goBckToMe" sender:self];
}

- (IBAction)msgTap:(id)sender {
}

- (IBAction)msgAddBtnTap:(id)sender {
}
@end
