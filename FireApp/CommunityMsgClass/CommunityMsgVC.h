//
//  CommunityMsgVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 01/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommunityMsgVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
- (IBAction)fireFeedTap:(id)sender;
- (IBAction)meTap:(id)sender;
- (IBAction)msgTap:(id)sender;
- (IBAction)msgAddBtnTap:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *msgTable;


@end
