//
//  CommunityFriendVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 02/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommunityFriendVC : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *FrndImgVw;
@property (strong, nonatomic) IBOutlet UILabel *FrndNmLbl;
@property (strong, nonatomic) IBOutlet UILabel *FrndStatusLbl;
@property (strong, nonatomic) IBOutlet UILabel *numOfFrnds;
@property (strong, nonatomic) IBOutlet UILabel *numOfFuelPoints;
@property (strong, nonatomic) IBOutlet UILabel *numOfMoments;
@property (strong, nonatomic) IBOutlet UILabel *BucketListLbl;
@property (strong, nonatomic) IBOutlet UILabel *bucketListPercentageLbl;
- (IBAction)frndBckBtn:(id)sender;

@end
