//
//  LoginVC.m
//  FireApp
//
//  Created by Nivendru Gavaskar on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "LoginVC.h"

@interface LoginVC ()

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"Enter");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTapNewUser:(id)sender {
    [self performSegueWithIdentifier:@"pushToRegister" sender:nil];
}
- (IBAction)didTapLogin:(id)sender {
    [self performSegueWithIdentifier:@"pushToDirectTrackProgress" sender:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
