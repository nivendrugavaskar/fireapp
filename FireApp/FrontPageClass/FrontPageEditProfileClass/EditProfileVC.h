//
//  EditProfileVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 03/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileVC : UIViewController
- (IBAction)EditProfileBckBtnTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *editProfileImageVw;
@property (strong, nonatomic) IBOutlet UILabel *firstNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *surNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *locationLbl;
@property (strong, nonatomic) IBOutlet UILabel *emailLbl;
@property (strong, nonatomic) IBOutlet UILabel *location1Lbl;
- (IBAction)syncFaceBook:(id)sender;
- (IBAction)doneTap:(id)sender;


@end
