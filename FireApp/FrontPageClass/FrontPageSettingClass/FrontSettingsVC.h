//
//  FrontSettingsVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 03/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FrontSettingsVC : UIViewController
- (IBAction)settingsBckBtn:(id)sender;
- (IBAction)DoneBtnTap:(id)sender;

@end
