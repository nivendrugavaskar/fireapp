//
//  NewMomentVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 03/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewMomentVC : UIViewController
- (IBAction)BckBtn:(id)sender;
- (IBAction)DoneBtnTap:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *momentTxt;
@property (strong, nonatomic) IBOutlet UILabel *VisibilityStatLbl;
@property (strong, nonatomic) IBOutlet UIImageView *momentImgVw;

@end
