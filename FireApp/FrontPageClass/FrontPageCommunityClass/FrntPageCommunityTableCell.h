//
//  FrntPageCommunityTableCell.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 03/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FrntPageCommunityTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *frntTableCellImgVw;
@property (strong, nonatomic) IBOutlet UILabel *frntTableCellLbl;
@property (strong, nonatomic) IBOutlet UIButton *frntTableCellBtn;

@end
