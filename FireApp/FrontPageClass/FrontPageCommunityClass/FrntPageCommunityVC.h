//
//  FrntPageCommunityVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 03/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FrntPageCommunityVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
- (IBAction)FrntBckBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *FrntProfileImgVw;
@property (strong, nonatomic) IBOutlet UILabel *numOfActionLbl;
@property (strong, nonatomic) IBOutlet UILabel *todaysActionLbl;
@property (strong, nonatomic) IBOutlet UILabel *numOfFuelPoints;
@property (strong, nonatomic) IBOutlet UILabel *fuelPointsLbl;
@property (strong, nonatomic) IBOutlet UILabel *numOfMomentsLbl;
@property (strong, nonatomic) IBOutlet UILabel *momentsLbl;
@property (strong, nonatomic) IBOutlet UITextField *createMomentTxtFld;
- (IBAction)createMomentAddBtn:(id)sender;
- (IBAction)allMomentsBtnTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *photosBtnTap;
@property (strong, nonatomic) IBOutlet UIButton *achievedBtnTap;
@property (strong, nonatomic) IBOutlet UITableView *FrntTableVw;

@end
