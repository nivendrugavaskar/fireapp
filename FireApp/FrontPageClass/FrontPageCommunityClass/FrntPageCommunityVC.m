//
//  FrntPageCommunityVC.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 03/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "FrntPageCommunityVC.h"
#import "FrntPageCommunityTableCell.h"
@interface FrntPageCommunityVC ()

@end

@implementation FrntPageCommunityVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _FrntProfileImgVw.image=[UIImage imageNamed:@"profile.jpg"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
static NSString *cellid=@"CommCell";
    FrntPageCommunityTableCell *cell=(FrntPageCommunityTableCell*)[tableView dequeueReusableCellWithIdentifier:cellid];
    cell.frntTableCellImgVw.image=[UIImage imageNamed:@"profile.jpg"];
    cell.frntTableCellLbl.text=@"Jhon";
    //Btn Action need to implemented.
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)FrntBckBtn:(id)sender
{
    [self performSegueWithIdentifier:@"pushToFrontSettings" sender:self];
}
- (IBAction)createMomentAddBtn:(id)sender {
    [self performSegueWithIdentifier:@"pushToCreateNewMoment" sender:self];
}

- (IBAction)allMomentsBtnTap:(id)sender {
}
@end
