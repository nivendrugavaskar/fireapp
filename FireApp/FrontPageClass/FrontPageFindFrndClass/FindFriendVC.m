//
//  FindFriendVC.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 03/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "FindFriendVC.h"

@interface FindFriendVC ()

@end

@implementation FindFriendVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)FindFrndBckBtnTap:(id)sender
{
    [self performSegueWithIdentifier:@"goBackToSettings" sender:self];
}

- (IBAction)DoneTap:(id)sender {
}

- (IBAction)SrchByEmailTap:(id)sender {
}

- (IBAction)SrchByFaceBookTap:(id)sender {
}

- (IBAction)SrchByNameTap:(id)sender {
}
@end
