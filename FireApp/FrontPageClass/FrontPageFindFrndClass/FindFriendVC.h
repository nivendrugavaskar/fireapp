//
//  FindFriendVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 03/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindFriendVC : UIViewController
- (IBAction)FindFrndBckBtnTap:(id)sender;
- (IBAction)DoneTap:(id)sender;
- (IBAction)SrchByEmailTap:(id)sender;
- (IBAction)SrchByFaceBookTap:(id)sender;
- (IBAction)SrchByNameTap:(id)sender;


@end
