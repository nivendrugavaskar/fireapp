//
//  OverComingSizeVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverComingSizeVC : UIViewController
- (IBAction)weightBtnTap:(id)sender;
- (IBAction)moneyBtnTap:(id)sender;
- (IBAction)otherBtnTap:(id)sender;
- (IBAction)sizeBtnTap:(id)sender;


@end
