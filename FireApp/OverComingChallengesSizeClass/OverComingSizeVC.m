//
//  OverComingSizeVC.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "OverComingSizeVC.h"

@interface OverComingSizeVC ()

@end

@implementation OverComingSizeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)weightBtnTap:(id)sender {
    [self performSegueWithIdentifier:@"pushToWeightFromSize" sender:self];
}

- (IBAction)moneyBtnTap:(id)sender {
    [self performSegueWithIdentifier:@"pushToMoneyFromSize" sender:self];
}

- (IBAction)otherBtnTap:(id)sender {
    [self performSegueWithIdentifier:@"pushToOtherFromSize" sender:self];
}

- (IBAction)sizeBtnTap:(id)sender {
    [self performSegueWithIdentifier:@"pushToTakeActionVC" sender:self];
}
@end
