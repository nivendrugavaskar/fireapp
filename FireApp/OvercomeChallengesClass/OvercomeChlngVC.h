//
//  OvercomeChlngVC.h
//  FireApp
//
//  Created by Nivendru Gavaskar on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OvercomeChlngVC : UIViewController
- (IBAction)weightTap:(id)sender;
- (IBAction)sizeTap:(id)sender;
- (IBAction)moneyTap:(id)sender;
- (IBAction)otherTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *otherBtn;
@property (strong, nonatomic) IBOutlet UIButton *moneyBtn;
@property (strong, nonatomic) IBOutlet UIButton *sizeBtn;
@property (strong, nonatomic) IBOutlet UIButton *weightBtn;

@end
