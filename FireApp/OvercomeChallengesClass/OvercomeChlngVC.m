//
//  OvercomeChlngVC.m
//  FireApp
//
//  Created by Nivendru Gavaskar on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "OvercomeChlngVC.h"

@interface OvercomeChlngVC ()

@end

@implementation OvercomeChlngVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //[[_otherBtn layer]setCornerRadius:1.0f];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)weightTap:(id)sender {
    [self performSegueWithIdentifier:@"OverComingWeight" sender:self];
}

- (IBAction)sizeTap:(id)sender {
    [self performSegueWithIdentifier:@"OverComingSize" sender:self];
}

- (IBAction)moneyTap:(id)sender {
    [self performSegueWithIdentifier:@"OverComingMoney" sender:self];
}

- (IBAction)otherTap:(id)sender {
    [self performSegueWithIdentifier:@"OverComingOther" sender:self];
}
@end
