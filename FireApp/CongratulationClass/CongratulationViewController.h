//
//  CongratulationViewController.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CongratulationViewController : UIViewController
- (IBAction)fireTap:(id)sender;

@end
