//
//  OverComingChallengesWeightVC.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "OverComingChallengesWeightVC.h"

@interface OverComingChallengesWeightVC ()

@end

@implementation OverComingChallengesWeightVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)SizeTap:(id)sender {
    [self performSegueWithIdentifier:@"pushToSize" sender:self];
}

- (IBAction)MoneyTap:(id)sender {
    [self performSegueWithIdentifier:@"pushToMoney" sender:self];
}

- (IBAction)OtherTap:(id)sender {
    [self performSegueWithIdentifier:@"pushToOther" sender:self];
}

- (IBAction)weightTap:(id)sender {
    [self performSegueWithIdentifier:@"pushTakeAction" sender:self];
}
@end
