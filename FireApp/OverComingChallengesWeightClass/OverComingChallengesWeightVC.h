//
//  OverComingChallengesWeightVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverComingChallengesWeightVC : UIViewController
- (IBAction)SizeTap:(id)sender;
- (IBAction)MoneyTap:(id)sender;
- (IBAction)OtherTap:(id)sender;
- (IBAction)weightTap:(id)sender;


@end
