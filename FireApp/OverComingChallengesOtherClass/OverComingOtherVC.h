//
//  OverComingOtherVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverComingOtherVC : UIViewController
- (IBAction)weightBtnTap:(id)sender;
- (IBAction)sizeBtnTap:(id)sender;

- (IBAction)moneyBtnTap:(id)sender;
- (IBAction)otherBtnTap:(id)sender;


@end
