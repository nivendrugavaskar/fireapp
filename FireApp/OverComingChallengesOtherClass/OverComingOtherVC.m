//
//  OverComingOtherVC.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 30/11/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "OverComingOtherVC.h"

@interface OverComingOtherVC ()

@end

@implementation OverComingOtherVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)weightBtnTap:(id)sender
{
    [self performSegueWithIdentifier:@"OverComingWeightSeg" sender:self];
}

- (IBAction)sizeBtnTap:(id)sender
{
    [self performSegueWithIdentifier:@"OverComingSizeSeg" sender:self];
}
- (IBAction)moneyBtnTap:(id)sender
{
    [self performSegueWithIdentifier:@"OverComingMoneySeg" sender:self];
}

- (IBAction)otherBtnTap:(id)sender {
    [self performSegueWithIdentifier:@"pushToTakeAction" sender:self];
}
@end
