//
//  CreativeLibraryCreatorsVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 03/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreativeLibraryCreatorsVC : UIViewController
- (IBAction)CategoryBtnTap:(id)sender;
- (IBAction)WeeklyPickTap:(id)sender;

@end
