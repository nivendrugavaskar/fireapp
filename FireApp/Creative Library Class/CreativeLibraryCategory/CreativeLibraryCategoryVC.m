//
//  CreativeLibraryCategoryVC.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 03/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "CreativeLibraryCategoryVC.h"

@interface CreativeLibraryCategoryVC ()

@end

@implementation CreativeLibraryCategoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)CreatorsBtnTap:(id)sender
{
    [self performSegueWithIdentifier:@"pushToCreator" sender:self];
}

- (IBAction)WeeklyPickTap:(id)sender
{
    [self performSegueWithIdentifier:@"pushToWeeklyPick" sender:self];
}
-(IBAction)OutComeTap:(id)sender
{
[self performSegueWithIdentifier:@"pushToOutCome" sender:self];
}
@end
