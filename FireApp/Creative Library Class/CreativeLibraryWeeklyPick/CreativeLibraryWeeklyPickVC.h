//
//  CreativeLibraryWeeklyPickVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 03/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreativeLibraryWeeklyPickVC : UIViewController
- (IBAction)CategoryTap:(id)sender;
- (IBAction)CreatorsTap:(id)sender;

@end
