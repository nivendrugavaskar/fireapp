//
//  CommunityFrndTableCell.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 02/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommunityFrndTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *FrndTblCellImgVw;
@property (strong, nonatomic) IBOutlet UILabel *FrndTablNmLbl;
@property (strong, nonatomic) IBOutlet UIButton *FrndTblAddBtn;

@end
