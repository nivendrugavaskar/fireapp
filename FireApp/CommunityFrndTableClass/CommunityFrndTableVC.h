//
//  CommunityFrndTableVC.h
//  FireApp
//
//  Created by Subhadeep Chakraborty on 02/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommunityFrndTableVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *FrndTblImgVw;
@property (strong, nonatomic) IBOutlet UILabel *FrndNmLbl;
@property (strong, nonatomic) IBOutlet UILabel *FrndStatLbl;
@property (strong, nonatomic) IBOutlet UILabel *numOfFrndLbl;
@property (strong, nonatomic) IBOutlet UILabel *numOfFuelPointLbl;
@property (strong, nonatomic) IBOutlet UILabel *numOfMomentLbl;
@property (strong, nonatomic) IBOutlet UITableView *frndTable;

@end
