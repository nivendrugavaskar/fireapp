//
//  CommunityFrndTableCell.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 02/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "CommunityFrndTableCell.h"

@implementation CommunityFrndTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
