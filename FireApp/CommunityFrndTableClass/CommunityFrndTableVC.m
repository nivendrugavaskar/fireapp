//
//  CommunityFrndTableVC.m
//  FireApp
//
//  Created by Subhadeep Chakraborty on 02/12/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "CommunityFrndTableVC.h"
#import "CommunityFrndTableCell.h"
@interface CommunityFrndTableVC ()

@end

@implementation CommunityFrndTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"FriendCell";
    CommunityFrndTableCell *cell=(CommunityFrndTableCell*)[tableView dequeueReusableCellWithIdentifier:cellid];
    cell.FrndTblCellImgVw.image=[UIImage imageNamed:@"profile.jpg"];
    cell.FrndTablNmLbl.text=@"Jerry";
    
    return cell;
}
-(IBAction)frndBtnBckTap:(id)sender
{

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
